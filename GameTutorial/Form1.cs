﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX.DirectInput;
using Microsoft.DirectX;

namespace GameTutorial
{
    public partial class Form1 : Form
    {
        Microsoft.DirectX.Direct3D.Device device;
        Microsoft.DirectX.DirectInput.Device keyboard;
        Microsoft.DirectX.Direct3D.Font font;
        Texture texture;
        Texture texture2;
        int x = 0, y = 0;
        float rotation = 0f;
        int fps = 0, frames = 0;
        long timeStarted = Environment.TickCount;
        Thread thread;
        
        // Camera
        float cameraX, cameraY, cameraZ;

        public Form1()
        {
            InitializeComponent();
            InitDevice();
            InitFont();
            InitInputDevice();
            LoadTexture();
        }

        private void UpdateCamera()
        {
            cameraX = x;
            cameraY = y;

            //device.Transform.Projection = Matrix.OrthoLH(
            //    device.Viewport.Width,
            //    device.Viewport.Height,
            //    0.1f,
            //    1000f);
            //device.Transform.View = Matrix.LookAtLH(
            //    new Vector3(cameraX, cameraY, 50f),
            //    new Vector3(x, y, 0f),
            //    new Vector3(0f, -1f, 0f));

            device.Transform.Projection = Matrix.PerspectiveFovLH(
                (int)Math.PI / 3,
                device.Viewport.Width / device.Viewport.Height,
                0.1f, 1000f);
            device.Transform.View = Matrix.LookAtLH(
                new Vector3(cameraX, cameraY, 500f),
                new Vector3(x, y, 0f),
                new Vector3(-1f, -1f, 0f));
        }

        private void InitFont()
        {
            System.Drawing.Font f = new System.Drawing.Font("Arial", 16f, FontStyle.Regular);
            font = new Microsoft.DirectX.Direct3D.Font(device, f);
        }

        private void LoadTexture()
        {
            // #1
            //texture = TextureLoader.FromFile(device, "TilesPlain0089_1_S.jpg");

            texture = TextureLoader.FromFile(device, "TilesPlain0089_1_S.jpg", 400, 400, 1, 0, Format.A8B8G8R8, Pool.Managed, Filter.Point, Filter.Point, Color.Transparent.ToArgb());
            texture2 = TextureLoader.FromFile(device, "BlackMan.png", 500, 250, 1, 0, Format.A8B8G8R8, Pool.Managed, Filter.Point, Filter.Point, Color.White.ToArgb());
        }

        private void InitDevice()
        {
            PresentParameters pp = new PresentParameters();
            pp.Windowed = true;
            pp.SwapEffect = SwapEffect.Discard;

            device = new Microsoft.DirectX.Direct3D.Device(0, Microsoft.DirectX.Direct3D.DeviceType.Hardware, this, CreateFlags.HardwareVertexProcessing, pp);
        }

        private void InitInputDevice()
        {
            keyboard = new Microsoft.DirectX.DirectInput.Device(SystemGuid.Keyboard);
            keyboard.SetCooperativeLevel(this, CooperativeLevelFlags.NonExclusive | CooperativeLevelFlags.Background);
            keyboard.Acquire();
        }

        private void UpdateInput()
        {
            foreach (Key k in keyboard.GetPressedKeys())
            {
                if (k == Key.D)
                {
                    x += 2;
                }

                if (k == Key.S)
                {
                    y += 2;
                }

                if (k == Key.A)
                {
                    x -= 2;
                }

                if (k == Key.W)
                {
                    y -= 2;
                }

                if (k == Key.Left)
                {
                    rotation -= 0.1f;
                }

                if (k == Key.Right)
                {
                    rotation += 0.1f;
                }
            }
        }

        private void Render()
        {
            while (true)
            {
                UpdateInput();

                device.Clear(ClearFlags.Target, Color.CornflowerBlue, 0, 1);
                // TODO: Exception when Resize the window
                device.BeginScene();
                {
                    using (Sprite s = new Sprite(device))
                    {
                        s.Begin(SpriteFlags.AlphaBlend);
                        {
                            s.Draw2D(
                                texture,
                                new Rectangle(0, 0, 0, 0),
                                new Rectangle(0, 0, device.Viewport.Width,
                                    device.Viewport.Height),
                                new Point(0, 0), 0f,
                                new Point(0, 0),
                                Color.White);

                            Matrix matrix = new Matrix();
                            matrix = Matrix.Transformation2D(
                                new Vector2(0f, 0f),
                                0f,
                                new Vector2(1f, 1f),
                                new Vector2(x + 75, y + 75),
                                rotation,
                                new Vector2(0f, 0f));
                            s.Transform = matrix;

                            //s.Draw2D(
                            //    texture2,
                            //    new Rectangle(0, 0, 0, 0),
                            //    new Rectangle(0, 0, 125, 125),
                            //    new Point(0, 0), 0f,
                            //    new Point(x, y),
                            //    Color.White);

                            s.Draw(
                                texture2,
                                new Rectangle(0, 125, 125, 125),
                                new Vector3(0, 0, 0),
                                new Vector3(x, y, 0f),
                                Color.White);
                            UpdateCamera();
                        }
                        s.End();
                    }

                    using (Sprite b = new Sprite(device))
                    {
                        b.Begin(SpriteFlags.AlphaBlend);
                        {
                            font.DrawText(b, "Golden Crow", new Point(0, 0), Color.Beige);
                            font.DrawText(b, fps + " FPS", new Point(0, 30), Color.Beige);
                        }
                        b.End();
                    }
                }
                device.EndScene();
                device.Present();

                if (Environment.TickCount >= timeStarted + 1000)
                {
                    fps = frames;
                    frames = 0;
                    timeStarted = Environment.TickCount;
                }
                frames++;
            }
        }

        // Used before adding Thread
        //private void timer1_Tick(object sender, EventArgs e)
        //{
        //    UpdateInput();
        //    Render();
        //}

        private void StartThread()
        {
            thread = new Thread(new ThreadStart(Render));
            thread.Start();
        }

        private void StopThread()
        {
            thread.Abort();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            StartThread();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopThread();
        }
    }
}
